import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import processing.core.*;

import processing.core.PVector;

public class Mover {

	private PVector position;
	private PVector velocity;
	private PVector acceleration;		// force = mass * acceleration | acceleration = mass / force
	private PVector walls;
	private float mass;
	private List<PVector> locationHistory;
	
	private int[] rgb = {randomIntRange(1, 224), randomIntRange(1, 224), randomIntRange(1, 224)};
	
	public Mover() {
		// Randomly generate initial values
		mass = randomFloatRange(1.0f, 2.5f);
		position = new PVector(randomFloatRange(52.0f, 400.0f), randomFloatRange(52.0f, 400.0f));
		velocity = new PVector(randomFloatRange(0.05f, 0.10f), randomFloatRange(0.05f, 0.10f));
		acceleration = new PVector(0,0);
		walls = new PVector(500,500);
		locationHistory = new ArrayList<>();
	}
	public Mover(PVector walls) {
		// Randomly generate initial values
		mass = randomFloatRange(1.0f, 2.5f);
		position = new PVector(randomFloatRange(mass*10, walls.x-50), randomFloatRange(mass*10, walls.y-50));
		velocity = new PVector(randomFloatRange(.001f, .01f), randomFloatRange(.01f, .001f));
		acceleration = new PVector(0,0);
		this.walls = walls;
		locationHistory = new ArrayList<>();
	}
	
	public void applyForce(PVector force) {
		PVector f = force.copy();
		f.div(mass);
		acceleration.add(f);
	}
	
	public void update() {
		// Track location before updating
		locationHistory.add(position.copy());
		// Collision detection
		collisionDetectionWalls();
		velocity.add(acceleration);
		velocity.limit(50);
		position.add(velocity);
		// Reset acceleration
		acceleration.mult(0);
		
		if(locationHistory.size() > 15) {
			locationHistory.remove(0);
		}
	}
	
	public List<PVector> getLocationHistory(){
		return locationHistory;
	}
	
	public void collisionDetectionWalls() {
		if(position.x + mass*10 >= walls.x) {
			// Ball has hit right or left wall
			// Turn around
			position.x = walls.x - (mass*10);
			velocity.x *= -1;
		} else if(position.x - mass*10 <= 0) {
			// Ball has hit right or left wall
			// Turn around
			position.x = 0 + (mass*10);
			velocity.x *= -1;
		}
		if(position.y + mass*10 >= walls.y) {
			// Ball has hit top or bottom wall
			// Turn around
			position.y = walls.y - (mass*10);
			velocity.y *= -1;
		} else if(position.y - mass*10 <= 0) {
			// Ball has hit top or bottom wall
			// Turn around
			position.y = 0 + (mass*10);
			velocity.y *= -1;
		}
	}
	
	public float randomFloatRange(float min, float max) {
		Random r = new Random();
		return min + (max - min) * r.nextFloat();
	}
	
	
	public PVector getPosition() {
		return position;
	}
	
	public PVector getVelocity() {
		return velocity;
	}
	
	public float getMass() {
		return mass;
	}
	
	public PVector getVelocityHeading() {
		// Direction of velocity unit vector
		PVector direction = velocity.copy().normalize();
		// Scale according to mass
		direction.mult(mass*10);
		// Move to position of ball
		return direction;
	}
	
	public void move(PVector distance) {
		position.add(distance);
	}
	
	public int randomIntRange(int min, int max) {
		Random r = new Random();
		int randomIntInRange = r.nextInt((max - min) + 1) + min;
		return randomIntInRange;
	}
	
	public int[] getRGB() {
		return rgb;
	}
	
}
