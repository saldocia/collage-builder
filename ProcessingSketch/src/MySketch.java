import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import processing.core.*;

public class MySketch extends PApplet{
	PVector window;
	ArrayList<Mover> moverList;
	int moverCnt = 200;
	int[] backgroundRgb = {50, 0, 125};
	
	static int imgCnt = 1;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PApplet.main("MySketch");

	}
	
	public void settings() {
		this.size(700, 500);
		smooth();
	}
	
	public void setup() {

		background(backgroundRgb[0], backgroundRgb[1], backgroundRgb[2]);
		//Stroke settings
		strokeWeight(2);
		stroke(255, 0, 0);
		
		// Initialize 
		window = new PVector(width, height);
		moverList = new ArrayList<Mover>();
		for(int i = 0; i < moverCnt; i++) {
			moverList.add(new Mover(window));
		}
		
	}
	
	public boolean collisionDetected(Mover a, Mover b) {
		// Calculate distance between positions, subtract scaled mass to find edges
		PVector aPos = a.getPosition().copy();
		PVector bPos = b.getPosition().copy();
		float aMass = a.getMass();
		float bMass = b.getMass();
		float distance = PVector.dist(aPos, bPos) - (aMass*10) - (bMass*10);
		
		return distance <= 0;
	}
	
	public void draw() {
		
		slowDownAnimation();
		if(mousePressed) { saveFrame(); }
		// Draw blank background over previous frame
		background(backgroundRgb[0], backgroundRgb[1], backgroundRgb[2]);
		
		// Made up force
		PVector gravity = new PVector(0f,0.2f);
		
		// Apply made up forces to all movers
		for(Mover a : moverList) {
			a.applyForce(gravity);

			// Collision detection with other movers
			// Should eventually move to inside of mover class
			for(Mover b : moverList) {
				// Compare positions with other movers
				if(a != b) {
					// Local variables for simplicity and readability
					PVector aPos = a.getPosition().copy();
					PVector bPos = b.getPosition().copy();
					PVector aDirection;
					PVector bDirection;
					float aMass = a.getMass();
					float bMass = b.getMass();
					float distance = PVector.dist(aPos, bPos) - (aMass*10) - (bMass*10);
					
					// Collision detected
					if(distance < 0) {
						// Forces occur in pairs
						// Unit vector of direction force will be applied
						// Scaled according to mass
						aDirection = PVector.sub(bPos, aPos).normalize();
						bDirection = PVector.sub(aPos, bPos).normalize();
						a.applyForce(bDirection.mult(bMass));
						b.applyForce(aDirection.mult(aMass));
					}
				}
			}
			 
			// Update mover
			a.update();
			// Change display color to mover color
			int[] rgb = a.getRGB();
			stroke(rgb[0], rgb[1], rgb[2]);
			fill(rgb[0], rgb[1], rgb[2]);
			strokeWeight(1+a.getMass());
			
			// Draw 
			List<PVector> history = a.getLocationHistory();
			for(int i = 0; i < history.size()-2; i++ ) {
				PVector p1 = history.get(i);
				PVector p2 = history.get(i+1);
				line(p1.x, p1.y, p2.x, p2.y);
			}
			
			PVector p = a.getPosition().copy();
			// Direction of velocity unit vector
			PVector l = a.getVelocity().copy().normalize();
			// Scale according to mass
			l.mult(a.getMass()*5);
			// Move to position of ball
			l.add(p);
		}
	}
	
	public void slowDownAnimation() {
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void saveFrame() {
		try {
			Thread.sleep(5000);
			imgCnt += 1;
			saveFrame("image" + System.currentTimeMillis());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public int randomIntRange(int min, int max) {
		Random r = new Random();
		int randomIntInRange = r.nextInt((max - min) + 1) + min;
		return randomIntInRange;
	}
}
